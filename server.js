var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.get("/clientes/:idclientes", function(req,res){
  res.send("Aqui tiene al cliente nùmero: "+req.params.idclientes);
})

app.post("/", function(req,res){
  res.send("Recibimos su peticiòn");
})

app.put("/", function(req, res) {
  res.send("Recibimos su peticiòn de modificaciòn Chaoo");
})

app.delete("/", function(req, res) {
  res.send("Recibimos");
})
